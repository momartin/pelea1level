/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peleame.nodes;

import org.pelea.core.communication.Message;
import org.pelea.core.communication.Messages;
import org.pelea.core.communication.connector.Connector;
import org.pelea.core.configuration.configuration;
import org.pelea.core.nodes.Module;
import org.pelea.core.nodes.RMI.RMIModule;
import org.pelea.implementations.Learning;
import org.peleame.nodes.infoLearning.*;

/**
 *
 * @author moises
 */
public class LearningBasic extends RMIModule implements Learning
{
    private final CaseBase cb;
    
    public LearningBasic(String name) throws Exception
    {
        super(name, "LEARNING", Connector.CLIENT);
        
        this.name = name;
        this.cb = new CaseBase(configuration.getInstance().getParameter(this.name, "CASE_BASE_NAME"), configuration.getInstance().getParameter(this.name, "PREDICATE_LIST").split(","), configuration.getInstance().getParameter(this.name, "PREDICATE_WEIGHTS").split(","));
   }

    @Override
    public String getAction(String state) 
    {
        return cb.getAction(state);
    }
    
    public String learnTuple(String tuple)
    {
        cb.addCase(tuple);
        
        return tuple;
    }

    @Override
    public Message messageHandler(Message recieve) throws Exception {
    
        switch (recieve.getTypeMsg()) {
            
            case Messages.MSG_START:
                return null;
            case Messages.MSG_REQACTION:
                return new Message(this.commumicationModel.getType(), Messages.NODE_MONITORING, Messages.MSG_REQACTION_RES, this.getAction(recieve.getContent()));
            case Messages.MSG_LEARNINGTUPLE:
                return new Message(this.commumicationModel.getType(), Messages.NODE_MONITORING, Messages.MSG_LEARNINGTUPLE_RES, this.getAction(recieve.getContent()));
            case Messages.MSG_STOP:
                if (this.isRunning()) this.state = Module.STOPPING;
                return null;
        }
        return null;
    }

    @Override
    public void errorHandler() throws Exception {
    }
}