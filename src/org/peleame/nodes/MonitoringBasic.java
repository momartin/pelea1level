package org.peleame.nodes;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.rmi.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.jdom.JDOMException;
import org.pelea.core.communication.*;
import org.pelea.core.communication.connector.Connector;
import org.pelea.core.communication.connector.RMIConnector;
import org.pelea.core.configuration.configuration;
import org.pelea.core.nodes.Module;
import org.pelea.core.nodes.RMI.RMIModule;
import org.pelea.implementations.Monitoring;
import org.pelea.languages.pddl.plan.ActionPlan;
import org.pelea.utils.Util;
import org.pelea.utils.experimenter.Experiment;
import org.pelea.utils.experimenter.ExperimentPool;
import org.peleame.nodes.infoMonitoring.dataMonitoring;
import org.peleame.nodes.infoMonitoring.infoMonitoring;
import org.peleame.nodes.response.response;
import pddl2xml.Pddl2XML;

/**
 *
 * @author moises
 */
public class MonitoringBasic extends RMIModule implements Monitoring
{
    private final dataMonitoring data;
    private final infoMonitoring info;
    private final String code;
    private final boolean validateState;
    private int answers; 
    private final boolean initExecution;
    private final boolean pddlIden;
    private final boolean useHorizon;
    private final ExperimentPool pool;
    
    private final int rounds;
    private int round;
    
    private final String resultFile;
    
    private long time;
    private long maxTime;
    
    public MonitoringBasic(String name) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NotBoundException, MalformedURLException, RemoteException, Exception
    {
        super(name, "MONITORING", Connector.SERVER);
        
        int numProblems = configuration.getInstance().getParameter("GENERAL", "PROBLEM").split(",").length;
        this.useHorizon = Boolean.parseBoolean(configuration.getInstance().getParameter("GENERAL", "USE_HORIZON"));
        
        this.code           = configuration.getInstance().getParameter(this.name, "EXECUTION_CODE").toUpperCase();
        this.data           = new dataMonitoring(configuration.getInstance().getParameter("GENERAL", "DOMAIN"));
        this.validateState  = Boolean.parseBoolean(configuration.getInstance().getParameter(this.name, "VALIDATE_STATE"));
        this.answers        = 0;    
        this.initExecution  = Boolean.parseBoolean(configuration.getInstance().getParameter(this.name, "INITEXECUTION"));
        this.pddlIden       = Boolean.parseBoolean(configuration.getInstance().getParameter(this.name, "PDDL_IDENTIFICATION")); 
        
        this.rounds = Integer.parseInt(configuration.getInstance().getParameter("GENERAL", "ROUNDS")) * numProblems;
        this.info = (this.initExecution) ? new infoMonitoring(Pddl2XML.convertDomain(configuration.getInstance().getParameter("GENERAL", "DOMAIN")), configuration.getInstance().getParameter(this.getName(), "COMPARISON_CLASS")):new infoMonitoring(configuration.getInstance().getParameter(this.getName(), "COMPARISON_CLASS"));
        this.pool = new ExperimentPool(configuration.getInstance().getParameter("GENERAL", "NAME"));
        this.round = 0;
        this.maxTime = 1000000; //1000 seconds
        this.maxTimeExecution = 86400000; // 86400 seconds = 24 hours.
        
        SimpleDateFormat df = new SimpleDateFormat ("HH_mm_ss_dd_MM_yyyy");
        
        this.resultFile = configuration.getInstance().getParameter("GENERAL", "TEMP_DIR") + "experiment" + df.format(new Date()) + ".xml";
    }
    
    public MonitoringBasic(String name, String masterPID) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NotBoundException, MalformedURLException, RemoteException, Exception
    {
        super(name, "MONITORING", Connector.SERVER, masterPID);
        
        int numProblems = configuration.getInstance().getParameter("GENERAL", "PROBLEM").split(",").length;
        this.useHorizon = Boolean.parseBoolean(configuration.getInstance().getParameter("GENERAL", "USE_HORIZON"));
        
        this.code           = configuration.getInstance().getParameter(this.name, "EXECUTION_CODE").toUpperCase();
        this.data           = new dataMonitoring(configuration.getInstance().getParameter("GENERAL", "DOMAIN"));
        this.validateState  = Boolean.parseBoolean(configuration.getInstance().getParameter(this.name, "VALIDATE_STATE"));
        this.answers        = 0;    
        this.initExecution  = Boolean.parseBoolean(configuration.getInstance().getParameter(this.name, "INITEXECUTION"));
        this.pddlIden       = Boolean.parseBoolean(configuration.getInstance().getParameter(this.name, "PDDL_IDENTIFICATION")); 
        
        this.rounds = Integer.parseInt(configuration.getInstance().getParameter("GENERAL", "ROUNDS")) * numProblems;
        this.info = (this.initExecution) ? new infoMonitoring(Pddl2XML.convertDomain(configuration.getInstance().getParameter("GENERAL", "DOMAIN")), configuration.getInstance().getParameter(this.getName(), "COMPARISON_CLASS")):new infoMonitoring(configuration.getInstance().getParameter(this.getName(), "COMPARISON_CLASS"));
        this.pool = new ExperimentPool(configuration.getInstance().getParameter("GENERAL", "NAME"));
        this.round = 0;
        this.maxTime = 1000000; //1000 seconds
        this.maxTimeExecution = 86400000; // 86400 seconds = 24 hours.
        
        SimpleDateFormat df = new SimpleDateFormat ("HH_mm_ss_dd_MM_yyyy");
        
        this.resultFile = "experiment" + this.getMasterPID() + "/experiment" + df.format(new Date()) + ".xml";
    }
    
    private boolean waitingAnswers()
    {
        return (this.answers != 0);
    }
    
    private boolean matches(ArrayList<String> idems, String name)
    {
        for (int i = 0; i < idems.size(); i++)
        {
            if (idems.get(i).matches(name)) {
                return true;
            }
        }
        
        return false;
    }

    @Override
    public String getPlanL(String stateL, String problem, String domainH, String domainL, String planL) 
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    private void initialMessage()
    {
        Message send = new Message(this.commumicationModel.getType(), Messages.NODE_EXECUTION, Messages.MSG_GETSENSORS, null);
        this.answers = ((RMIConnector) this.commumicationModel).sendMessage(send);
    }
    
    private void finishRound() throws IOException {
        
        this.round++;
        
        //Save result and finish execution       
        Util.printDebug(this.name, "ROUND: " + this.round);
        Util.printDebug(this.name, "ROUNDS: " + this.rounds);
        
        if (this.round >= this.rounds) {
                        
            this.pool.saveXML(this.resultFile, this.info.getDomainName(), this.info.getProblemName());
            this.state = Module.STOPPING;
        }
        else {
            this.answers = 0; //Reset answers from execution 
            Message send = new Message(this.commumicationModel.getType(), Messages.NODE_EXECUTION, Messages.MSG_FINISH, null);
            ((RMIConnector) this.commumicationModel).sendMessage(send);
        }
    }
    
    private void solveProblem(Message msg) throws RemoteException, IOException, JDOMException
    {
        Experiment experiment = new Experiment();
        
        this.pool.addExperiment(experiment);

        response res = new response(msg.getContent());
        
        this.info.resetPlan(); //Remove last plan
        this.info.loadState(res.getNode("problem", false));
        this.data.setStateH(this.info.getProblemXML());
        this.info.loadDomain(res.getNode("domain", false));
        //this.data.setDomainH(this.info.getDomainXML());
        
        Message send = new Message(this.commumicationModel.getType(), Messages.NODE_DECISSIONSUPPORT, Messages.MSG_GETPLANINFO, this.data.buildXML(true, false, true, false, false, false, false));
        ((RMIConnector) this.commumicationModel).sendMessage(send);
    }

    private void processState(Message msg) throws RemoteException, IOException, JDOMException
    {
        response res = new response(msg.getContent());

        if (this.info.isPlan())
        {
            this.info.generateNextState();
            
            //Compare new state for environment with the newState generated previosly.
            if ((this.info.isValidState(res.getNode("state", false), true, true, this.validateState)))
            {
                this.pool.executeAction();

                //Save new state as current state
                this.data.setStateH(this.info.getProblemXML());

                if (this.info.moreActions()) {
                    
                    if (this.info.goalsReached()){
                        Util.printDebug(this.getName(), "Problem Solved");
                        this.pool.finishExperiment(Experiment.SOLVED);
                        this.finishRound();
                    }
                    else
                        this.executeAction();
                }
                else {
                    if (this.info.goalsReached()){
                        Util.printDebug(this.getName(), "Problem Solved");
                        this.pool.finishExperiment(Experiment.SOLVED);
                        this.finishRound();
                    }
                    else {
                        Util.printDebug(this.getName(), "Replanning 2");
                        
                        this.data.setStateH(this.info.getProblemXML());
                        this.data.setPlanH(this.info.getPlanXML(this.info.actionsExecuted()-1));
                        
                        //Reset expected answers
                        this.answers = 0;
                        
                        //Delete wrong actions
                        this.info.deletePlan();
                        
                        //Generate a new plan of actions
                        Message send = new Message(this.commumicationModel.getType(), Messages.NODE_DECISSIONSUPPORT, Messages.MSG_REPAIRORPEPLAN, this.data.buildXML(true, false, true, false, true, false, false));
                        ((RMIConnector) this.commumicationModel).sendMessage(send);
                    }
                }
            }
            else {
                //Load real state
                this.info.loadState(res.getNode("state", false));
                this.data.setStateH(this.info.getProblemXML());
                this.data.setPlanH(this.info.getPlanXML(this.info.actionsExecuted()));

                Util.printDebug(this.getName(), "Replanning 1");

                //Reset expected answers
                this.answers = 0;

                //Delete wrong actions
                this.info.deletePlan();

                //Generate a new plan of actions
                Message send = new Message(this.commumicationModel.getType(), Messages.NODE_DECISSIONSUPPORT, Messages.MSG_REPAIRORPEPLAN, this.data.buildXML(true, false, true, false, true, false, false));
                ((RMIConnector) this.commumicationModel).sendMessage(send);
            }
        }
        else
        { 
            //Initial state sended by execution nodes, it is loaded as initial state.
            this.info.loadState(res.getNode("state", false));  
            Message send = new Message(this.commumicationModel.getType(), Messages.NODE_DECISSIONSUPPORT, Messages.MSG_GETPLANINFO, this.data.buildXML(true, false, true, false, false, false, false));
            /*this.answers = */((RMIConnector) this.commumicationModel).sendMessage(send);
        }
    }
    
    private boolean executeAction() throws RemoteException
    {                              
        //ArrayList references    = (ArrayList) this.me.getReferenceListByType(messages.NODE_EXECUTION);
        ActionPlan ap = this.info.getNextAction();
        Message send = null;
        
        for (int i = 0; i < ap.size(); i++) {
            if (this.pddlIden) {
                ArrayList<String> idems = ap.get(i).getValuesByType(this.code);
                send = new Message(this.commumicationModel.getType(), Messages.NODE_EXECUTION, Messages.MSG_EXECUTEPLAN, ap.getXmlByPosition(i));
                
                this.answers += this.answers = ((RMIConnector) this.commumicationModel).sendMessage(send, idems);
            }
            else {
                send = new Message(this.commumicationModel.getType(), Messages.NODE_EXECUTION, Messages.MSG_EXECUTEPLAN, ap.getXmlByPosition(i));
                this.answers += this.answers = ((RMIConnector) this.commumicationModel).sendMessage(send);
            }
        }
            
        return true;                                       
    } 
    
    private void processPlan(Message msg) throws RemoteException, JDOMException, IOException
    {
        response res = new response(msg.getContent());
        
        //Loading plan of actions in info structure to analyze the execution of the accions
        String[] values = this.info.loadPlan(msg.getContent());
        this.data.setPlanH(res.getNode("plans", false));
        this.pool.addEpisode(values[0], values[1]);

        if (this.info.moreActions()) {
            this.executeAction();
        }
        else {
            Util.printDebug(this.getName(), "No more actions");
            this.pool.finishExperiment(Experiment.DEADEND);
            this.finishRound();
        }
    }
    
    @Override
    public Message messageHandler(Message recieve) throws Exception {
        
        this.time = System.currentTimeMillis();

        switch (recieve.getTypeMsg()) {
        
            case Messages.MSG_GETSENSORS_RES:
                //this.info.updateState(Util.getNodesFromXML(recieve.getContent(), new String[]{"problem"})[0]);
                this.answers--;
                if (!this.waitingAnswers()) {
                    this.processState(recieve);
                }
                break;
            case Messages.MSG_GETPLANINFO_RES:
                this.processPlan(recieve);
                break;
            case Messages.MSG_REPAIRORPEPLAN_RES:
                this.processPlan(recieve);
                break;
            case Messages.MSG_SOLVEPROBLEM:
                this.solveProblem(recieve);
                break;
        }
        return null;
    }

    @Override
    public void errorHandler() throws Exception {
        this.finishRound();
    }
}


