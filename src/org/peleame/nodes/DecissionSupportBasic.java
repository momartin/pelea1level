/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peleame.nodes;

import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;
import org.pelea.core.communication.Message;
import org.pelea.core.communication.Messages;
import org.pelea.core.communication.connector.Connector;
import org.pelea.core.configuration.configuration;
import org.pelea.core.nodes.Module;
import org.pelea.core.nodes.RMI.RMIModule;
import org.pelea.implementations.DecisionSupport;
import org.pelea.planner.Planner;
import org.pelea.utils.Util;
import org.peleame.nodes.response.response;

/**
 *
 * @author moises
 */
public class DecissionSupportBasic extends RMIModule implements DecisionSupport
{
    private final List<Planner> planners;
    
    public DecissionSupportBasic(String name) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NotBoundException, MalformedURLException, RemoteException
    {
        super(name, "DECISIONSUPPORT", Connector.CLIENT);
        
        this.planners   = new ArrayList();
        
        String codes[] =  configuration.getInstance().getParameter(this.name, "PLANNERS").split(",");
            
        for (int i = 0; i < codes.length; i++)
        {
            codes[i] = codes[i].trim();
                
            Util.printDebug(this.getName(), "INIT PLANNER " + configuration.getInstance().getParameter(codes[i], "PLANNER_NAME") + " IN MODE " + configuration.getInstance().getParameter(codes[i], "PLANNER_MODE"));
            Planner p = (Planner) (Class.forName(configuration.getInstance().getParameter(codes[i], "PLANNER_CLASS")).getConstructor(String.class)).newInstance(codes[i]);
                
            this.planners.add(p);
        }
    }
    
    private int getPlanner(int mode)
    {
        int position = 0;
        boolean find = false;
        
        while (position < this.planners.size())
        {
            if (this.planners.get(position).getMode() == mode)
            {
               return position; 
            }
            
            position++;
        }
        
        return 0;
    }

    @Override
    public String RepairOrReplan(String stateH, String domainH, String planH) {
        String plan     = "";
        int position    = 0;
        
        try 
        {    
            position    = this.getPlanner(Planner.MODE_REPLANING);
            plan        = this.planners.get(position).getRePlanH(domainH, stateH, planH);
        
            return plan;
        
        } 
        catch (Exception ex) 
        {
            Util.printError(this.getName(), "Generating plan of actions (" + ex.toString() + ")");
            
            plan += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
            plan += "<define xmlns=\"http://www.pelea.org/xPddl\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.pelea.org/xPddl xPddl.xsd\">";
            plan += "<error>";
            plan += "Replanning using planner " + this.planners.get(position).getName();
            plan += "</error>";
            plan += "</define>";
            
            return plan;
        }
    }

    @Override
    public String getPlanHInfoMonitor(String stateH, String domainH) {
        String plan     = "";
        int position    = 0;
        
        try {    
            position    = this.getPlanner(Planner.MODE_PLANING);
            plan = this.planners.get(position).getPlanH(domainH, stateH);
            
            return plan;
        } 
        catch (Exception ex) {
            Util.printError(this.getName(), "Generating plan of actions (" + ex.toString() + ")");
            
            plan += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
            plan += "<define xmlns=\"http://www.pelea.org/xPddl\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.pelea.org/xPddl xPddl.xsd\">";
            plan += "<error>";
            plan += "Generating plan of actions using planner " + this.planners.get(position).getName();
            plan += "</error>";
            plan += "</define>";
            
            return plan;
        }
    }

    @Override
    public Message messageHandler(Message recieve) throws Exception {
        
        response res;
        
        switch (recieve.getTypeMsg()) {
            case Messages.MSG_START:
                this.state = Module.RUNNING;
                return null;
            case Messages.MSG_GETPLANINFO:
                if (this.isRunning()) {
                    res = new response(recieve.getContent());
                    return new Message(this.commumicationModel.getType(), Messages.NODE_MONITORING, Messages.MSG_GETPLANINFO_RES, this.getPlanHInfoMonitor(res.getNode("problem", true), res.getNode("domain", true)));
                }
                break;
            case Messages.MSG_REPAIRORPEPLAN:
                if (this.isRunning()) { 
                    res = new response(recieve.getContent());
                    return new Message(this.commumicationModel.getType(), Messages.NODE_MONITORING, Messages.MSG_REPAIRORPEPLAN_RES, this.RepairOrReplan(res.getNode("problem", true), res.getNode("domain", true), res.getNode("plans", true)));
                }
                break;
            case Messages.MSG_STOP:
                if (this.isRunning()) this.state = Module.STOPPING;
                return null;
        }
        
        return null;
    }

    @Override
    public void errorHandler() throws Exception {
    }
}
