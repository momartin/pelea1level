/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peleame.nodes.infoLearning;

import java.io.StringReader;
import java.io.PrintWriter;
import java.io.FileWriter;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;
import java.util.regex.Pattern;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.pelea.core.configuration.configuration;
import org.pelea.languages.pddl.UtilPDDL;
import org.pelea.languages.pddl.structures.NodeList;
import org.pelea.languages.pddl.structures.NodeTree;
import org.pelea.languages.pddl.structures.nodes.Node;
import org.pelea.languages.pddl.structures.nodes.Predicate;
import org.pelea.utils.Util;

/**
 *
 * @author moises
 */
public class CaseBase
{
    private final ArrayList<Tuple> _tuples;
    private final String _fileName;
    private int _maxSize;
    private double _theta;
    private Hashtable<String,Double> predicateWeights;
    //int counter;
    
    public CaseBase(String fileName, String[] predicates, String[] weights)
    {
        this._tuples = new ArrayList<Tuple>();
        this._fileName = fileName;
        this._theta = .5;
        predicateWeights = new Hashtable<String,Double>();
        this.loadPredicateWeights(predicates, weights);
        this.loadCases();
        this.printPredicateWeights(predicates);
    }
    
    public void loadPredicateWeights(String[] predicates, String[] weights){
        for(int i = 0; i < predicates.length; i++){
            predicateWeights.put(predicates[i].toLowerCase(), Double.parseDouble(weights[i]));
        }
    }
    
    public void printPredicateWeights(String[] predicates){
        System.out.println("PREDICATE - WEIGHT");
        for(int i = 0; i < predicateWeights.size(); i++){
            System.out.println(predicates[i] + " " + predicateWeights.get(predicates[i]));
        }
        System.out.println("THETA: " + this._theta);
    }
    
    public int getSize()
    {
        return this._tuples.size();
    }
    
    public Tuple getCase(int index)
    {
        return this._tuples.get(index);
    }
    
    public String getAction(String state)
    {
        //System.out.println("ESTADO " + state);
        String p = "";
        if(this.getNodesFromXML(state, new String[]{"state"}, "state", true)[0] != null)
                p = this.getNodesFromXML(state, new String[]{"state"}, "state", true)[0];
        else
                p = this.getNodesFromXML(state, new String[]{"problem"}, "problem", true)[0];
        String s = this.getNodesFromXML(p, new String[]{"init"}, "state", true)[0];
        NodeList statePerceived = this.generateState(s);
        String g = this.getNodesFromXML(p, new String[]{"goals"}, "goals", true)[0];
        NodeTree goalsPerceived = this.generateTree(g);
        
        boolean findCase = false;
        String action = "", xml = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><define xmlns=\"http://www.pelea.org/xPddl\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.pelea.org/xPddl xPddl.xsd\">";
        for(int i = 0; i < this.getSize() && !findCase; i++)
        {
            NodeList stateCase = this.getCase(i).getState();
            NodeTree goalsCase = this.getCase(i).getGoals();
            if(stateCase.size() == statePerceived.size()){
                //findCase = matchState(stateCase, statePerceived) && matchGoals(goalsCase, goalsPerceived);
                findCase = (1.0 * distanceState1(stateCase, statePerceived) + 1.0 * distanceGoals1(goalsCase, goalsPerceived)) < this._theta;
                //System.out.println("ENCUENTRA CASO " + findCase);
                
                if(findCase) action = this.getCase(i).getAction();
            }
        }
        
        if(!findCase)
        {
            xml += p;
            xml += "</define>";
            //counter++;
            //this.writeFile("counter", counter + "", true);
            //System.out.println("NO ENCONTRADO: ");
            //System.out.println(xml);
        }
        else
        {
            xml += p;
            xml += action;
            xml += "</define>";
            //System.out.println("ENCONTRADO: ");
            //System.out.println(xml);
        }
        
        return xml;
    }
    
    
    public boolean matchState(NodeList stateCase, NodeList statePerceived)
    {
        boolean findCase = false;

        //System.out.println("COMPARA: ");
        //System.out.println(stateCase.getXml(false));
        //System.out.println(statePerceived.getXml(false));
        boolean equals = true;
        
        for(int j = 0; j < stateCase.size() && equals; j++){
            Node predicateStateCase = stateCase.get(j);
            boolean found = false;
            for(int k = 0; k < statePerceived.size() && !found; k++){
                Node predicateStatePerceived = statePerceived.get(k);
                // Compare predicate names
                boolean equalPredicates = false;
                if(((Predicate)predicateStateCase).getName().equalsIgnoreCase(((Predicate)predicateStatePerceived).getName()))
                {
                    // Compare predicate arguments
                    String[] arg1 = ((Predicate)predicateStateCase).getValues();
                    String[] arg2 = ((Predicate)predicateStatePerceived).getValues();
                    equalPredicates = true;
                    for(int h = 0; h < arg2.length && equalPredicates; h++){
                        String valuePerceived = arg2[h];
                        if(!valuePerceived.equalsIgnoreCase(arg1[h])){
                            equalPredicates = false;
                        }
                    }
                }
                if(equalPredicates) found = true;
            }
            if(!found){
                //System.out.println(predicateStateCase.getXml());
                equals = false;
            }
        }
        if(equals) 
        {
            findCase = true;
        }
        
        System.out.println("MATCH STATE " + findCase);
        
        return findCase;
    }
    
    public boolean matchGoals(NodeTree goalsCase, NodeTree goalsPerceived){
        boolean findCase = false;
        
        NodeList predicatesCase = new NodeList();
        goalsCase.getPredicates(predicatesCase);
        NodeList predicatesPerceived = new NodeList();
        goalsPerceived.getPredicates(predicatesPerceived);
        if(predicatesCase.size() == predicatesPerceived.size())
        {
            for(int i = 0; i < predicatesCase.size(); i++)
            {
                findCase = false;
                for(int j = 0; j < predicatesPerceived.size() && !findCase; j++){
                    if(predicatesCase.get(i).equal(predicatesPerceived.get(j))){
                        findCase = true;
                    }
                }
                if(!findCase) break;
            }
        }
        
        System.out.println("MATCH GOALS " + findCase);
        
        return findCase;    
    }
    
    public double distanceState1(NodeList stateCase, NodeList statePerceived)
    {
        boolean findCase = false;

        //System.out.println("COMPARA: ");
        //System.out.println(stateCase.getXml(false));
        //System.out.println(statePerceived.getXml(false));
        double totalDistance = 0;
        for(int j = 0; j < stateCase.size(); j++){
            Node predicateStateCase = stateCase.get(j);
            double predicateWeight = 0;
            if(this.predicateWeights.get(predicateStateCase.getName().toLowerCase()) != null) predicateWeight = this.predicateWeights.get(predicateStateCase.getName().toLowerCase());
            double distanceMin = predicateWeight;
            for(int k = 0; k < statePerceived.size() && distanceMin > 0; k++){
                Node predicateStatePerceived = statePerceived.get(k);
                // Compare predicate names
                boolean equalPredicates = false;
                double distance = predicateWeight;
                if(((Predicate)predicateStateCase).getName().equalsIgnoreCase(((Predicate)predicateStatePerceived).getName()))
                {
                    // Compare predicate arguments
                    distance = 0;
                    String[] arg1 = ((Predicate)predicateStateCase).getValues();
                    String[] arg2 = ((Predicate)predicateStatePerceived).getValues();
                    for(int h = 0; h < arg2.length; h++){
                        String valuePerceived = arg2[h];
                        if(!valuePerceived.equalsIgnoreCase(arg1[h])){
                            distance += (double) predicateWeight/arg2.length;
                        }
                    }
                }
                /*System.out.println("COMPARA: ");
                System.out.println(predicateStatePerceived.getXml());
                System.out.println(predicateStateCase.getXml());
                System.out.println(distance);*/
                if(distanceMin > distance) distanceMin = distance;
            }
            //System.out.println("MINIMA: " + distanceMin);
            
            totalDistance += distanceMin;
        }
        
        System.out.println("DISTANCIA TOTAL ESTADO: " + totalDistance);
        
        return totalDistance;
    }
    
    public double distanceGoals1(NodeTree goalsCase, NodeTree goalsPerceived){
        NodeList predicatesCase = new NodeList();
        goalsCase.getPredicates(predicatesCase);
        NodeList predicatesPerceived = new NodeList();
        goalsPerceived.getPredicates(predicatesPerceived);
        double totalDistance = 0;
        for(int i = 0; i < predicatesCase.size(); i++)
        {
            Node predicateGoalCase = predicatesCase.get(i);
            double predicateWeight = 0;
            if(this.predicateWeights.get(predicateGoalCase.getName().toLowerCase()) != null) predicateWeight = this.predicateWeights.get(predicateGoalCase.getName().toLowerCase());
            double distanceMin = predicateWeight;
            for(int j = 0; j < predicatesPerceived.size() && distanceMin > 0; j++){
                Node predicateGoalPerceived = predicatesPerceived.get(j);
                // Compare predicate names
                double distance = predicateWeight;
                if(((Predicate)predicateGoalCase).getName().equalsIgnoreCase(((Predicate)predicateGoalPerceived).getName()))
                {
                    // Compare predicate arguments
                    distance = 0;
                    String[] arg1 = ((Predicate)predicateGoalCase).getValues();
                    String[] arg2 = ((Predicate)predicateGoalPerceived).getValues();
                    for(int h = 0; h < arg2.length; h++){
                        String valuePerceived = arg2[h];
                        if(!valuePerceived.equalsIgnoreCase(arg1[h])){
                            distance += (double) predicateWeight/arg2.length;
                        }
                    }
                }
                if(distanceMin > distance) distanceMin = distance;
            }
            totalDistance += distanceMin;
        }
        
        System.out.println("DISTANCIA TOTAL METAS: " + totalDistance);
        
        return totalDistance;
    }
    
    private void loadCases()
    {
        try {
            BufferedReader br = new BufferedReader(new FileReader(new File (this._fileName)));
            String c = new String();
            while((c=br.readLine())!=null){
                // Analyze the case
                // State
                String state  = this.getNodesFromXML(c, new String[]{"state"}, "state", true)[0];
                NodeList st = this.generateState(state);
                // Goals
                String g  = this.getNodesFromXML(c, new String[]{"goals"}, "goals", true)[0];
                NodeTree gt = (NodeTree) this.generateTree(g);
                // Action
                String action = this.getNodesFromXML(c, new String[]{"action-plan"}, "action-plan", true)[0];
                Tuple t = new Tuple(st, gt, action);
                this._tuples.add(t);
            }
            br.close();
        }
        catch(Exception e){
            System.out.println("[Learning] Se ha producido un error al cargar los casos de " + this._fileName);
        }
    }
    
    public void addCase(String tuple)
    {
        // State
        String p = "";
        if(this.getNodesFromXML(tuple, new String[]{"state"}, "state", true)[0] != null)
                p = this.getNodesFromXML(tuple, new String[]{"state"}, "state", true)[0];
        else
                p = this.getNodesFromXML(tuple, new String[]{"problem"}, "problem", true)[0];
        String s = this.getNodesFromXML(p, new String[]{"init"}, "state", true)[0];
        NodeList st = this.generateState(s);
        // Goal
        String g = this.getNodesFromXML(p, new String[]{"goals"}, "goals", true)[0];
        NodeTree gt = this.generateTree(g);
        // Action
        String action = this.getNodesFromXML(tuple, new String[]{"action-plan"}, "action-plan", true)[0];
        action = action.replaceAll("\n", "");
        action = action.replaceAll("\r", "");
        Tuple t = new Tuple(st, gt, action);
        this._tuples.add(t);
        this.saveCases();
    }
    
    private void saveCases()
    {
        try
        {
            PrintWriter pw = new PrintWriter(new FileWriter(this._fileName));

            for(int i = 0; i < this.getSize(); i++){
                String xmlCase = "<case>";
                // State
                xmlCase += "<state>" + this.getCase(i).getState().getXml(false) + "</state>";
                // Goals
                xmlCase += "<goals>" + this.getCase(i).getGoals().getXml() + "</goals>";
                // Action
                String action = this.getCase(i).getAction();
                action = action.replaceAll("\n", "");
                action = action.replaceAll("\r", "");
                xmlCase += action;
                xmlCase += "</case>";
                pw.println(xmlCase);
            }
            
            pw.close();
            
        } catch (Exception e) {
            System.out.println("[Learning] Se ha producido un error al guardar los casos en la base de casos");
        } 
    }
    
    private NodeList generateState(String state)
    {
        NodeList st = new NodeList();
        try
        {
            
            XMLOutputter outputter  = new XMLOutputter(Format.getPrettyFormat());
            SAXBuilder builder      = new SAXBuilder();
            Document document       = (Document) builder.build(new StringReader(state));
            List nodes              = document.getRootElement().getChildren();
            Element node            = null;
            //TODO: It should be recursive, but nodes always are in the first level.
            
            for (int i = 0; i < nodes.size(); i++) {
                st.addNode((Element) nodes.get(i));
            }
         }
         catch (Exception ex)
         {
            Util.printError("[Learning]", ex.toString());
            return null;
         }
        
        return st;
    }
    
    private NodeTree generateTree(String goals){
        try
        {
            
            XMLOutputter outputter  = new XMLOutputter(Format.getPrettyFormat());
            SAXBuilder builder      = new SAXBuilder();
            Document document       = (Document) builder.build(new StringReader(goals));
            List elements              = document.getRootElement().getChildren();
         
            //System.out.println("GOALS " + goals);
            return (NodeTree)this.generateTree(elements).get(0);
         }
         catch (Exception ex)
         {
            Util.printError("[Learning]", ex.toString());
            return null;
         }
    }
    
    private List generateTree(List elements)
    {
        List values         = null;
        List nodes          = new ArrayList();
        NodeTree new_node   = null;
        
        for (int i = 0; i < elements.size(); i++) 
        {
            Element element = (Element) elements.get(i);
            switch (UtilPDDL.getType(element.getAttributeValue("type"), element.getName()))
            {
                case UtilPDDL.NODE_PREDICATE:
                    new_node = new NodeTree(element);
                    break;
                case UtilPDDL.NODE_FUNCTION:
                    new_node = new NodeTree(element);
                    break;
                /*case UtilPDDL.NODE_EXIST:
                    new_node = new NodeTree(element);
                    Connector node = (Connector) new_node.getNode();
                    List els = (List)element.getChildren();
                    for(int j = 0; j < els.size(); j++)
                    {
                        Element el = (Element) els.get(j);
                        if(UtilPDDL.getType(el.getAttributeValue("type"), el.getName()) == UtilPDDL.NODE_NOT_DEFINED)
                        {
                            node.addParameter(el.getAttributeValue("name"));
                        }
                        else
                        {
                            ArrayList l = new ArrayList();
                            l.add(el);
                            new_node.addDescendents(generateTree(l));
                        }
                    }
                    break;*/
                default:
                    new_node = new NodeTree(element);
                    new_node.addDescendents(generateTree(element.getChildren()));
                    break;
            }

            nodes.add(nodes.size(), new_node);
        }
        
        return nodes;
    }
    
    
    private String[] getNodesFromXML(String xml, String[] tags, String n, boolean omithead)
    {
        String content[] = new String[tags.length];
        
        try
        {
            
            XMLOutputter outputter  = new XMLOutputter(Format.getPrettyFormat());
            SAXBuilder builder      = new SAXBuilder();
            Document document       = (Document) builder.build(new StringReader(xml));
            List nodes              = document.getRootElement().getChildren();
            Element node            = null;
            //TODO: It should be recursive, but nodes always are in the first level.
            
            for (int i = 0; i < nodes.size(); i++)
            {
                node = (Element) nodes.get(i);
                for (int j = 0; j < tags.length; j++)
                {
                    if (node.getName().toUpperCase().matches(tags[j].toUpperCase()))
                    {
                        ((Element) nodes.get(i)).setName(n);
                        content[j] = "";
                        if(!omithead) content[j] = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?><define xmlns=\"http://www.pelea.org/xPddl\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.pelea.org/xPddl xPddl.xsd\">";
                        content[j] += outputter.outputString((Element) nodes.get(i));
                        if(!omithead) content[j] += "</define>";
                        
                        break; //TODO i dont like.
                    }
                }
            }
            return content;
                            
         }
         catch (Exception ex)
         {
            Util.printError("[Learning]", ex.toString());
            return null;
         }
    }
    
    /*public void writeFile(String name, String s, boolean append){
        try
        {
            PrintWriter pw = new PrintWriter(new FileWriter(name, append));
            pw.println(s);
            pw.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
    
}
