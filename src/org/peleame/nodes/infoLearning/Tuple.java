/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peleame.nodes.infoLearning;

import org.pelea.languages.pddl.structures.NodeList;
import org.pelea.languages.pddl.structures.NodeTree;

/**
 *
 * @author moises
 */
public class Tuple
{
    private NodeList _state;
    private NodeTree _goals;
    private String _action;
    
    public Tuple(NodeList state, NodeTree goals, String action)
    {
        this._state = state;
        this._goals = goals;
        this._action = action;
    }
    
    public NodeList getState()
    {
        return this._state;
    }
    
    public void setState(NodeList state)
    {
        this._state = state;
    }
    
    public NodeTree getGoals()
    {
        return this._goals;
    }
    
    public void setGoals(NodeTree goals)
    {
        this._goals = goals;
    }
    
    public String getAction(){
        return this._action;
    }
    
    public void setAction(String action){
        this._action = action;
    }
}
