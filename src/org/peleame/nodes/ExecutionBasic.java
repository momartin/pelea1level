/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peleame.nodes;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.net.Socket;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.pelea.core.communication.Message;
import org.pelea.core.communication.Messages;
import org.pelea.core.communication.connector.Connector;
import org.pelea.core.configuration.configuration;
import org.pelea.core.nodes.Module;
import org.pelea.core.nodes.RMI.RMIModule;
import org.pelea.implementations.Execution;
import org.pelea.utils.Util;
import org.peleame.nodes.response.response;
import pddl2xml.Pddl2XML;

/**
 *
 * @author moises
 */

public class ExecutionBasic extends RMIModule implements Execution
{ 
    private final Socket connection;
    private final PrintStream out;
    private final BufferedReader in;
    private final boolean _initExecution;
    private final boolean _executionMode;


    public ExecutionBasic(String name) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NotBoundException, MalformedURLException, RemoteException, IOException
    {
        super(name, "EXECUTION", Connector.CLIENT);
        
        this.connection     = new Socket( configuration.getInstance().getParameter(this.name, "IP") , Integer.parseInt(configuration.getInstance().getParameter(this.name, "PORT")) );
        this.out            = new PrintStream(this.connection.getOutputStream());
        this.in             = new BufferedReader(new InputStreamReader(this.connection.getInputStream()));
        this._initExecution = Boolean.parseBoolean(configuration.getInstance().getParameter(this.name, "INITEXECUTION"));     
        this._executionMode = Boolean.parseBoolean(configuration.getInstance().getParameter(this.name, "EXECUTIONMODE"));      
    }

    @Override
    public void executePlan(String planL) 
    {
	try 
        {
            XMLOutputter outputter  = new XMLOutputter(Format.getPrettyFormat());
            SAXBuilder builder      = new SAXBuilder();
            Document document       = (Document) builder.build(new StringReader(planL));
            Element plan            = ((Element) document.getRootElement());        
            List actions            = plan.getChildren();
        
            for (int i = 0; i < actions.size(); i++) {
                this.executeAction(outputter.outputString((Element) actions.get(i)));
            }
        } 
        catch (Exception e) 
        {
           Util.printError(this.getName(), "Executing plan of actions");
	}
    }

    @Override
    public void executeAction(String actionL) 
    {
        char[] buffer = new char[4096];
        int bytes     = 0;
        String xml    = ""; 
        
        if (this._executionMode)
        {
            try 
            {
                this.out.print("92");
                this.out.flush();

                Util.printDebug(this.getName(), "Sending command 92");

                this.out.print(actionL);
                this.out.flush();
                
                Util.printDebug(this.getName(), actionL);

                bytes = this.in.read(buffer);

                if (bytes > 0) {
                    xml += new String (buffer, 0, bytes);
                }
                else
                {
                    xml += "<error>";
                    xml += "Error getting answer from robot";
                    xml += "</error>";
                }
            }
            catch (IOException ex) 
            {
                xml += "<error>";
                xml += "Error getting answer from robot";
                xml += "</error>";
            }
            catch (Exception ex)
            {
                Util.printError(this.getName(), ex.toString());
            }
        }
        else
        {
            Util.printDebug(this.getName(), "Executing action: " + actionL);
        }   
        
        
    }

    @Override
    public void executeAction(double time, String action) 
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getSensors() 
    {
        char[] buffer = new char[4096];
        int bytes     = 0;
        String xml    = ""; 

        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
        xml += "<define xmlns=\"http://www.pelea.org/xPddl\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.pelea.org/xPddl xPddl.xsd\">";
        
        try 
        {
            this.out.print("94");
            this.out.flush();
            
            Util.printDebug(this.getName(), "Sending command 94");
            
            bytes = this.in.read(buffer);
            
            if (bytes > 0)
            {
                xml += "<state level=\"l\" problem=\"" + "Problem Name" + "\" domain=\"" + "Domain name" + "\">";
                xml += new String (buffer, 0, bytes);
                xml += "</state>";
            }
            else
            {
                xml += "<error>";
                xml += "Error getting answer from robot";
                xml += "</error>";
            }
        }
        catch (IOException ex) 
        {
            xml += "<error>";
            xml += "Error getting answer from robot";
            xml += "</error>";
        }
        
        xml += "</define>";
        
        return xml;
    }

    @Override
    public String getSensorsWithTime(double instant_time) 
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public double getLastTime() 
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String solveProblem() 
    {
        String xml      = "";
        response res    = new response();
        
        try 
        {
            res.addNode("domain", Pddl2XML.convertDomain(configuration.getInstance().getParameter("GENERAL", "DOMAIN")));
            res.addNode("problem", Pddl2XML.convertProblem(configuration.getInstance().getParameter("GENERAL", "PROBLEM")));
            
            xml = res.generateResponse();
        } 
        catch (Exception ex) 
        {
            xml += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
            xml += "<define xmlns=\"http://www.pelea.org/xPddl\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.pelea.org/xPddl xPddl.xsd\">";
            xml += "<error>";
            xml += "Error getting actual state";
            xml += "</error>";
            xml += "</define>";
        }
        
        return xml;
    }

    @Override
    public Message messageHandler(Message recieve) throws Exception {
        
        switch (recieve.getTypeMsg()) {
            case Messages.MSG_START: 
                if (this._initExecution) {
                    return new Message(this.commumicationModel.getType(), Messages.NODE_MONITORING, Messages.MSG_SOLVEPROBLEM, this.solveProblem());
                }
                return null;
            case Messages.MSG_STOP:
                this.state = Module.STOPPING;
                return null;
            case Messages.MSG_EXECUTEPLAN: 
                this.executePlan(recieve.getContent());
                return new Message(this.commumicationModel.getType(), Messages.NODE_MONITORING, Messages.MSG_GETSENSORS_RES, this.getSensors());
            case Messages.MSG_EXECUTEACTION: 
                this.executeAction(recieve.getContent());
                return new Message(this.commumicationModel.getType(), Messages.NODE_MONITORING, Messages.MSG_GETSENSORS_RES, this.getSensors());
            case Messages.MSG_GETSENSORS: 
                return new Message(this.commumicationModel.getType(), Messages.NODE_MONITORING, Messages.MSG_GETSENSORS_RES, this.getSensors());
        }
        return null;
    }

    @Override
    public void errorHandler() throws Exception {
    }
}
