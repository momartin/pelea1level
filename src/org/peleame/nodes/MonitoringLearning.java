package org.peleame.nodes;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.rmi.*;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import org.jdom.JDOMException;
import org.pelea.core.communication.*;
import org.pelea.core.communication.connector.Connector;
import org.pelea.core.communication.connector.RMIConnector;
import org.pelea.core.configuration.configuration;
import org.pelea.core.nodes.Module;
import org.pelea.core.nodes.RMI.RMIModule;
import org.pelea.implementations.Monitoring;
import org.pelea.languages.pddl.plan.ActionPlan;
import org.pelea.utils.Util;
import org.pelea.utils.experimenter.Experiment;
import org.pelea.utils.experimenter.ExperimentPool;
import org.peleame.nodes.infoMonitoring.dataMonitoring;
import org.peleame.nodes.infoMonitoring.infoMonitoring;
import org.peleame.nodes.response.response;
import pddl2xml.Pddl2XML;

/**
 *
 * @author moises
 */
public class MonitoringLearning extends RMIModule implements Monitoring
{
    private final dataMonitoring data;
    private final infoMonitoring info;
    private final String code;
    private final boolean validateState;
    private int answers; 
    private final boolean initExecution;
    private final boolean pddlIden;
    private final boolean useHorizon;
    private final ExperimentPool pool;
    
    private boolean plan;
    
    private final int rounds;
    private int round;
    
    private final String resultFile;
    
    private long time;
    private long maxTime;
    
    public MonitoringLearning(String name) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NotBoundException, MalformedURLException, RemoteException, Exception
    {
        super(name, "MONITORING", Connector.SERVER);
        
        int numProblems = configuration.getInstance().getParameter("GENERAL", "PROBLEM").split(",").length;
        this.useHorizon = Boolean.parseBoolean(configuration.getInstance().getParameter("GENERAL", "USE_HORIZON"));
        
        this.code           = configuration.getInstance().getParameter(this.name, "EXECUTION_CODE").toUpperCase();
        this.data           = new dataMonitoring(configuration.getInstance().getParameter("GENERAL", "DOMAIN"));
        this.validateState  = Boolean.parseBoolean(configuration.getInstance().getParameter(this.name, "VALIDATE_STATE"));
        this.answers        = 0;    
        this.initExecution  = Boolean.parseBoolean(configuration.getInstance().getParameter(this.name, "INITEXECUTION"));
        this.pddlIden       = Boolean.parseBoolean(configuration.getInstance().getParameter(this.name, "PDDL_IDENTIFICATION")); 
        
        this.rounds = Integer.parseInt(configuration.getInstance().getParameter("GENERAL", "ROUNDS")) * numProblems;
        //this.learning = (((RMIConnector) this.commumicationModel).getNumNodes(Messages.NODE_LEARNING) > 0);
        this.plan = false;
        
        this.info = (this.initExecution) ? new infoMonitoring(Pddl2XML.convertDomain(configuration.getInstance().getParameter("GENERAL", "DOMAIN")), configuration.getInstance().getParameter(this.getName(), "COMPARISON_CLASS")):new infoMonitoring(configuration.getInstance().getParameter(this.getName(), "COMPARISON_CLASS"));
        this.pool = new ExperimentPool(configuration.getInstance().getParameter("GENERAL", "NAME"));
        this.round = 0;
        this.maxTime = 1000000; //1000 seconds
        
        SimpleDateFormat df = new SimpleDateFormat ("HH:mm:ss-dd-MM-yyyy");
        
        this.resultFile = configuration.getInstance().getParameter("GENERAL", "TEMP_DIR") + "results-" + df.format(new Date()) + ".xml";
    }
    
    private boolean waitingAnswers()
    {
        return (this.answers != 0);
    }
    
    private boolean matches(ArrayList<String> idems, String name)
    {
        for (int i = 0; i < idems.size(); i++)
        {
            if (idems.get(i).matches(name)) {
                return true;
            }
        }
        
        return false;
    }

    @Override
    public String getPlanL(String stateL, String problem, String domainH, String domainL, String planL) 
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }
    
    private void initialMessage()
    {
        Message send = new Message(this.commumicationModel.getType(), Messages.NODE_EXECUTION, Messages.MSG_GETSENSORS, null);
        this.answers = ((RMIConnector) this.commumicationModel).sendMessage(send);
    }
    
    private void finishRound() throws IOException {
        
        this.round++;
        
        //Save result and finish execution
        Util.printDebug(this.name, "ROUND: " + this.round);
        Util.printDebug(this.name, "ROUNDS: " + this.rounds);
        
        if (this.round >= this.rounds) {
                        
            this.pool.saveXML(this.resultFile, this.info.getDomainName(), this.info.getProblemName());
            this.state = Module.STOPPING;
        }
        else {
            this.answers = 0; //Reset answers from execution 
            Message send = new Message(this.commumicationModel.getType(), Messages.NODE_EXECUTION, Messages.MSG_FINISH, null);
            ((RMIConnector) this.commumicationModel).sendMessage(send);
        }
    }
    
    private void solveProblem(Message msg) throws RemoteException, IOException, JDOMException
    {
        Experiment experiment = new Experiment();
        
        this.pool.addExperiment(experiment);

        response res = new response(msg.getContent());
        
        this.info.resetPlan(); //Remove last plan
        this.info.loadState(res.getNode("problem", false));
        this.data.setStateH(this.info.getProblemXML());
        this.info.loadDomain(res.getNode("domain", false));
        //this.data.setDomainH(this.info.getDomainXML());
        
        Message send = new Message(this.commumicationModel.getType(), Messages.NODE_DECISSIONSUPPORT, Messages.MSG_GETPLANINFO, this.data.buildXML(true, false, true, false, false, false, false));
        ((RMIConnector) this.commumicationModel).sendMessage(send);
    }

    private void processState(Message msg) throws RemoteException, IOException, JDOMException
    {
        response res = new response(msg.getContent());

        if (this.info.isPlan())
        {
            this.info.generateNextState();
            
            //Compare new state for environment with the newState generated previosly.
            if ((this.info.isValidState(res.getNode("state", false), true, true, this.validateState)))
            {
                this.pool.executeAction();

                //Save new state as current state
                this.data.setStateH(this.info.getProblemXML());

                if (this.info.moreActions()) {
                    this.executeAction();
                }
                else {
                    if (this.info.goalsReached()){
                        Util.printDebug(this.getName(), "Problem Solved");
                        this.pool.finishExperiment(Experiment.SOLVED);
                    }
                    else {
                        Util.printDebug(this.getName(), "No more actions");
                        this.pool.finishExperiment(Experiment.DEADEND);
                    }

                    this.finishRound();
                }
            }
            else {
                
                 //Load real state
                    this.info.loadState(res.getNode("state", false));
                    this.data.setStateH(this.info.getProblemXML());

                    Util.printDebug(this.getName(), "Replanning");

                    //Reset expected answers
                    this.answers = 0;

                    //Delete wrong actions
                    this.info.deletePlan();

                    //Generate a new plan of actions
                    Message send = new Message(this.commumicationModel.getType(), Messages.NODE_DECISSIONSUPPORT, Messages.MSG_REPAIRORPEPLAN, this.data.buildXML(true, false, true, false, true, false, false));
                    ((RMIConnector) this.commumicationModel).sendMessage(send);
            }
        }
        else
        { 
            //Initial state sended by execution nodes, it is loaded as initial state.
            this.info.loadState(res.getNode("state", false));
                
            Message send = new Message(this.commumicationModel.getType(), Messages.NODE_DECISSIONSUPPORT, Messages.MSG_GETPLANINFO, this.data.buildXML(true, false, true, false, false, false, false));
            /*this.answers = */((RMIConnector) this.commumicationModel).sendMessage(send);
        }
    }
    
    private boolean executeAction() throws RemoteException
    {                              
        //ArrayList references    = (ArrayList) this.me.getReferenceListByType(messages.NODE_EXECUTION);
        ActionPlan ap = this.info.getNextAction();
        Message send = null;
        
        for (int i = 0; i < ap.size(); i++) {
            if (this.pddlIden) {
                ArrayList<String> idems = ap.get(i).getValuesByType(this.code);
                send = new Message(this.commumicationModel.getType(), Messages.NODE_EXECUTION, Messages.MSG_EXECUTEPLAN, ap.getXmlByPosition(i));
                
                this.answers += this.answers = ((RMIConnector) this.commumicationModel).sendMessage(send, idems);
            }
            else {
                send = new Message(this.commumicationModel.getType(), Messages.NODE_EXECUTION, Messages.MSG_EXECUTEPLAN, ap.getXmlByPosition(i));
                this.answers += this.answers = ((RMIConnector) this.commumicationModel).sendMessage(send);
            }
        }
            
        return true;                                       
    } 
    
    private void processPlan(Message msg) throws RemoteException, JDOMException, IOException
    {
        response res = new response(msg.getContent());
        this.plan = true;
        
        //Loading plan of actions in info structure to analyze the execution of the accions
        String[] values = this.info.loadPlan(res.getNode("plans", false));
        //Saving plan data in temporal buffer
        this.data.setPlanH(res.getNode("plans", false));
        this.pool.addEpisode(values[0], values[1]);

        if (this.info.moreActions()) {
            this.executeAction();
        }
        else {
            Util.printDebug(this.getName(), "No more actions");
        }
    }
    
    private void processActionLearning(Message msg) throws RemoteException, IOException, JDOMException {
        response res = new response(msg.getContent());

        if(this.info.goalsReached()) {
            Util.printDebug(this.getName(), "No more actions");
        }
        else {
            if(res.getNode("action-plan", false) == null)
            {
                // No action for this state
                if (this.info.isPlan())
                {
                    this.info.generateNextState();
                    
                    //Compare new state for environment with the newState generated previoslu.
                    //if (this.info.isValidStateRelaxed(this.info.getNodesFromXML(res.getNode("problem", false), new String[]{"init"}, "state", true)[0], true))
                    if ((this.info.isValidState(res.getNode("state", false), true, true, this.validateState)))
                    {
                        //Save new state as current state
                        this.data.setStateH(this.info.getProblemXML());
                        
                        if (this.info.moreActions())
                        {
                            this.learnAction();   
                        }
                        else 
                        {
                            Util.printDebug(this.getName(), "No more actions");
                            // Inform to the execution node no more actions in plan
                            //this.me.getReferenceByType(messages.NODE_DECISSIONSUPPORT).sendMessage(send);    
                        }
                    }
                    else
                    {
                        Util.printDebug(this.getName(), "Replanning");
                        
                        //Delete wrong actions
                        this.info.deletePlan();
                        
                        //Update the state
                        this.info.loadState(res.getNode("problem", false));
                        this.data.setStateH(this.info.getProblemXML());
                        
                        Message send = new Message(this.commumicationModel.getType(), Messages.NODE_DECISSIONSUPPORT, Messages.MSG_REPAIRORPEPLAN, this.data.buildXML(true, false, true, false, true, false, false));
                        ((RMIConnector) this.commumicationModel).sendMessage(send);
                    }
                }
                else
                {
                    //Update the state
                    this.info.loadState(res.getNode("problem", false));
                    this.data.setStateH(this.info.getProblemXML());
                    
                    Message send = new Message(this.commumicationModel.getType(), Messages.NODE_DECISSIONSUPPORT, Messages.MSG_GETPLANINFO, this.data.buildXML(true, false, true, false, false, false, false));
                    ((RMIConnector) this.commumicationModel).sendMessage(send);
                    
                    //message send = new message(this.getTypeNode(), this._name, messages.MSG_GETPLANINFO, this._data.buildXML(true, false, true, false, true, false, false));
                }
            }
            else
            {
                // There is an action for this state
                if (this.info.isPlan())
                {
                    this.info.generateNextState();
                    if ((this.info.isValidState(res.getNode("state", false), true, true, this.validateState)))
                    {
                        //Save new state as current state
                        this.data.setStateH(this.info.getProblemXML());
                        
                        if (this.info.moreActions()) {
                            this.executeAction();
                        }
                        else {
                            Util.printDebug(this.getName(), "No more actions");
                        }
                    }
                    else {
                        Message send = new Message(this.commumicationModel.getType(), Messages.NODE_EXECUTION, Messages.MSG_EXECUTEPLAN, res.getNode("action-plan", false));
                        this.answers += ((RMIConnector) this.commumicationModel).sendMessage(send);
                    }
                }
                else {
                    Message send = new Message(this.commumicationModel.getType(), Messages.NODE_EXECUTION, Messages.MSG_EXECUTEPLAN, res.getNode("action-plan", false));
                    this.answers += ((RMIConnector) this.commumicationModel).sendMessage(send);;
                }
            }
        }
    }
    
    private boolean learnAction() throws RemoteException {
        
        ActionPlan ap = this.info.getNextAction();
        
        if(ap.size() > 0)
        {
            //this.data.setAction(this._info.removeXMLHeader(ap.getXmlByPosition(0)));
            
            Message send = new Message(this.commumicationModel.getType(), Messages.NODE_LEARNING, Messages.MSG_LEARNINGTUPLE, this.data.buildXML(true, false, false, false, false, false, true));
            this.answers += ((RMIConnector) this.commumicationModel).sendMessage(send);;
            
            //Message send = new Message(this.getTypeNode(), this._name, messages.MSG_LEARNINGTUPLE, this._data.buildXML(true, false, false, false, false, false, true));
            //this.me.getReferenceByType(messages.NODE_LEARNING).sendMessage(send);
        }
        
        return true;
    }
    
    private void requestActionLearning(Message msg) throws IOException, JDOMException {
        
        response res = new response(msg.getContent());
        
        this.data.setStateH(res.getNode("state", false));
        
        Message send = new Message(this.commumicationModel.getType(), Messages.NODE_LEARNING, Messages.MSG_REQACTION, this.data.buildXML(true, false, false, false, false, false, false));
        ((RMIConnector) this.commumicationModel).sendMessage(send);
    }
    
    private void proccesInitialState(Message msg) throws IOException, RemoteException, JDOMException {
        response res = new response(msg.getContent());
        
        if(res.getNode("action-plan", false) == null) {
            this.learnAction();
        }
        else {
            this.executeAction();
        }
        
        this.plan = false;
    }

    @Override
    public Message messageHandler(Message recieve) throws Exception {
        
        this.time = System.currentTimeMillis();

        switch (recieve.getTypeMsg()) {
        
            case Messages.MSG_GETSENSORS_RES:
                //this.info.updateState(Util.getNodesFromXML(recieve.getContent(), new String[]{"problem"})[0]);
                this.answers--;
                if (!this.waitingAnswers()) {
                    this.requestActionLearning(recieve);
                }
                break;
            case Messages.MSG_REQACTION_RES:
                
                if (plan)
                    this.proccesInitialState(recieve);
                else
                    this.processState(recieve);
                
                this.processActionLearning(recieve);
                break;
            case Messages.MSG_LEARNINGTUPLE_RES:
                this.executeAction();
                break;
            case Messages.MSG_GETPLANINFO_RES:
                this.processPlan(recieve);
                break;
            case Messages.MSG_REPAIRORPEPLAN_RES:
                this.processPlan(recieve);
                break;
            case Messages.MSG_SOLVEPROBLEM:
                this.solveProblem(recieve);
                break;
        }
        return null;
    }

    @Override
    public void errorHandler() throws Exception {
        this.finishRound();
    }
}


