/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peleame.nodes;

import java.io.StringReader;
import java.lang.reflect.InvocationTargetException;
import java.net.MalformedURLException;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.List;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.input.SAXBuilder;
import org.jdom.output.Format;
import org.jdom.output.XMLOutputter;
import org.pelea.core.communication.Message;
import org.pelea.core.communication.Messages;
import org.pelea.core.communication.connector.Connector;
import org.pelea.core.communication.connector.RMIConnector;
import org.pelea.core.configuration.configuration;
import org.pelea.core.nodes.Module;
import org.pelea.core.nodes.RMI.RMIModule;
import org.pelea.implementations.Execution;
import org.pelea.utils.Util;
import org.peleame.nodes.response.response;
import pddl2xml.Pddl2XML;

/**
 *
 * @author moises
 */

public class ExecutionFiles extends RMIModule implements Execution
{ 
    private boolean _initExecution;

    public ExecutionFiles(String name) throws ClassNotFoundException, NoSuchMethodException, InstantiationException, IllegalAccessException, IllegalArgumentException, InvocationTargetException, NotBoundException, MalformedURLException, RemoteException
    {
        super(name, "EXECUTION", Connector.CLIENT);
        this._initExecution = Boolean.parseBoolean(configuration.getInstance().getParameter(this.name, "INITEXECUTION"));
    }
    
    @Override
    public void executePlan(String planL) 
    {
	try 
        {
            XMLOutputter outputter  = new XMLOutputter(Format.getPrettyFormat());
            SAXBuilder builder      = new SAXBuilder();
            Document document       = (Document) builder.build(new StringReader(planL));
            Element plan            = ((Element) document.getRootElement());        
            List actions            = plan.getChildren();
        
            for (int i = 0; i < actions.size(); i++) 
            {
                this.executeAction(outputter.outputString((Element) actions.get(i)));
            }
        
        } 
        catch (Exception e) 
        {
           Util.printError(this.getName(), "Executing plan of actions");
	}
    }

    @Override
    public void executeAction(String actionL) 
    {
        System.out.println("Executing action: " + actionL);
    }

    @Override
    public void executeAction(double time, String action) 
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String getSensors() 
    {
        XMLOutputter outputter  = new XMLOutputter(Format.getCompactFormat());
        SAXBuilder builder      = new SAXBuilder();
        Document document;
        
        String xml              = "";
        
        xml += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
        xml += "<define xmlns=\"http://www.pelea.org/xPddl\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.pelea.org/xPddl xPddl.xsd\">";
            
        try 
        {
            document = (Document) builder.build(new StringReader(Pddl2XML.convertDomain(configuration.getInstance().getParameter("GENERAL", "PROBLEM"))));
         
            xml += outputter.outputString((Element) document.getRootElement().getContent().get(1));

        } 
        catch (Exception ex) 
        { 
            xml += "<error>";
            xml += "Error getting answer from robot";
            xml += "</error>";
        }
        
        xml += "</define>";
        
        return xml;
    }

    @Override
    public String getSensorsWithTime(double instant_time) 
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public double getLastTime() 
    {
        throw new UnsupportedOperationException("Not supported yet.");
    }

    @Override
    public String solveProblem() 
    {
        String xml      = "";
        response res    = new response();
        
        try 
        {
            res.addNode("domain", Pddl2XML.convertDomain(configuration.getInstance().getParameter("GENERAL", "DOMAIN")));
            res.addNode("problem", Pddl2XML.convertProblem(configuration.getInstance().getParameter("GENERAL", "PROBLEM")));
            
            xml = res.generateResponse();
        } 
        catch (Exception ex) 
        {
            xml += "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>";
            xml += "<define xmlns=\"http://www.pelea.org/xPddl\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.pelea.org/xPddl xPddl.xsd\">";
            xml += "<error>";
            xml += "Error getting actual state";
            xml += "</error>";
            xml += "</define>";
        }
        
        return xml;
    }

    @Override
    public Message messageHandler(Message recieve) throws Exception {
        
        switch (recieve.getTypeMsg()) {
            case Messages.MSG_START: 
                if (this._initExecution) {
                    return new Message(this.commumicationModel.getType(), Messages.NODE_MONITORING, Messages.MSG_SOLVEPROBLEM, this.solveProblem());
                }
                return null;
            case Messages.MSG_STOP:
                this.state = Module.STOPPING;
                return null;
            case Messages.MSG_EXECUTEPLAN: 
                this.executePlan(recieve.getContent());
                return new Message(this.commumicationModel.getType(), Messages.NODE_MONITORING, Messages.MSG_GETSENSORS_RES, this.getSensors());
            case Messages.MSG_EXECUTEACTION: 
                this.executeAction(recieve.getContent());
                return new Message(this.commumicationModel.getType(), Messages.NODE_MONITORING, Messages.MSG_GETSENSORS_RES, this.getSensors());    
            case Messages.MSG_GETSENSORS: 
                return new Message(this.commumicationModel.getType(), Messages.NODE_MONITORING, Messages.MSG_GETSENSORS_RES, this.getSensors());
        }
        return null;
    }

    @Override
    public void errorHandler() throws Exception {
    }
}
