/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.peleame.nodes.error;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.pelea.languages.pddl.structures.NodeList;

/**
 *
 * @author moises
 */
public abstract class ErrorHandler {
    
    public int getErrorCode(String error) {
        
        return Integer.parseInt(error.substring(5));
    }
    
    public String[] getActionValues(String action) throws JDOMException, IOException {
        
        SAXBuilder builder  = new SAXBuilder();
        Document document   = (Document) builder.build(new StringReader(action));
        List<Element> childrens = document.getRootElement().getChildren();
                
        String[] values = new String[childrens.size()+1];
        
        values[0] = document.getRootElement().getAttributeValue("name");
        
        for (int i = 0; i < childrens.size(); i++) {
            values[i+1] = childrens.get(i).getAttributeValue("name");
        }
        
        return values;
    }
    
    public String generateError(String error) {
        
        String xml = "";
        
        xml += "<act>";
        xml += "<action>";
        xml += "<name>" + "remove_"+error.toLowerCase() + "</name>";
        xml += "</action>";
        xml += "</act>";
        
        return xml;
    }

    public abstract ArrayList generateDataError(NodeList predicates, String action) throws JDOMException, IOException;
}