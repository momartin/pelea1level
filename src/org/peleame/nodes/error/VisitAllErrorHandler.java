/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package org.peleame.nodes.error;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Random;
import org.jdom.JDOMException;
import org.pelea.languages.pddl.structures.NodeList;
import org.pelea.languages.pddl.structures.nodes.Node;
import org.pelea.languages.pddl.structures.nodes.Predicate;

/**
 *
 * @author moises
 */
public class VisitAllErrorHandler extends ErrorHandler {

    @Override
    public ArrayList generateDataError(NodeList predicates, String action) throws JDOMException, IOException {
        
        ArrayList<String> resultData = new ArrayList();
        String values[] = this.getActionValues(action);
        
        ArrayList<Node> candidates;
        int i, position;
        
        if (values[0].contains("move")) {
        
            candidates = (ArrayList<Node>) predicates.getNodesByNameAndValue("connected", new String[]{values[1]});
            i = 0;
            
            while (i < candidates.size()) {
                if (((Predicate) candidates.get(i)).getValue(1).toUpperCase().matches(values[2].toUpperCase()))
                    candidates.remove(i);
                else
                    i++;
            }
            
            if (candidates.size() > 0) {
               position = (candidates.size() > 1) ? (new Random()).nextInt(candidates.size()):0;
               resultData.add(((Predicate) candidates.get(position)).getValue(1));
            }
            else { //origin position
                resultData.add(values[1].toLowerCase());
            }
            
            candidates = (ArrayList<Node>) predicates.getNodesByNameAndValue("connected", new String[]{values[2]});
           
            i = 0;
            
            while (i < candidates.size()) {
                if (((Predicate) candidates.get(i)).getValue(1).toUpperCase().matches(values[1].toUpperCase()))
                    candidates.remove(i);
                else
                    i++;
            }
            
            if (candidates.size() > 0) {
               position = (candidates.size() > 1) ? (new Random()).nextInt(candidates.size()):0;
               resultData.add(((Predicate) candidates.get(position)).getValue(1));
            }
            else { //destiny position
                resultData.add(values[1].toLowerCase());
            }
        }
        
        //return resultData;
        return new ArrayList<String>();
    }
}
