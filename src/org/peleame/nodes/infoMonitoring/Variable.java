/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peleame.nodes.infoMonitoring;

/**
 *
 * @author moises
 */
public class Variable 
{
    private String name;
    private String value;
    
    public Variable(String name) {
        this.name = name;
    }
    
    public Variable(String name, String value) {
        this.name = name;
        this.value = value;
    }
    
    public String getName() {
        return this.name;
    }
    
    public void setName(String name) {
        this.name = name;
    }
    
    public String getValue() {
        return this.value;
    }
    
    public void setValue(String value) {
        this.value = value;
    }
}
