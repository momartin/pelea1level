/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.peleame.nodes.infoMonitoring;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;
import org.jdom.Document;
import org.jdom.Element;
import org.jdom.JDOMException;
import org.jdom.input.SAXBuilder;
import org.pelea.languages.pddl.PDDLDomain;
import org.pelea.languages.pddl.PDDLPlan;
import org.pelea.languages.pddl.PDDLProblem;
import org.pelea.languages.pddl.comparison.PDDLComparison;
import org.pelea.languages.pddl.plan.Action;
import org.pelea.languages.pddl.plan.ActionPlan;
import org.pelea.languages.pddl.predicates.actions.ActionResult;
import org.pelea.languages.pddl.structures.NodeList;
import org.pelea.utils.Util;

/**
 *
 * @author moises
 */
public class infoMonitoring 
{
    private String name;
    
    private final PDDLDomain domain;
    private final PDDLProblem problem;
    private PDDLPlan plan;
    private PDDLComparison comparison;
    
    private int cursor;
    private List<String> values;
    
    public infoMonitoring(String comparisonClass) throws Exception
    {
        this.name = "Info Monitoring PDDL";
        this.domain = new PDDLDomain();
        this.problem = new PDDLProblem();
        this.plan = null;
        this.comparison = (PDDLComparison) Class.forName(comparisonClass).getConstructor().newInstance();
    }
    
    public infoMonitoring(String domain, String comparisonClass) throws Exception
    {
         this.name = "Info Monitoring PDDL";
        this.domain = new PDDLDomain(domain);
        this.problem = new PDDLProblem();
        this.plan = null;
        this.comparison = (PDDLComparison) Class.forName(comparisonClass).getConstructor().newInstance();
    }
    
    public infoMonitoring(String domain, String problem, String comparisonClass) throws Exception
    {
        this.name = "Info Monitoring PDDL";
        this.domain = new PDDLDomain(domain);
        this.problem = new PDDLProblem(problem);
        this.plan = null;
        this.comparison = (PDDLComparison) Class.forName(comparisonClass).getConstructor().newInstance();
    }
    
    public final String getName()
    {
        return this.name;
    }
    
    public void setName(String name)
    {
        this.name = name;
    }
    
    public String[] loadPlan(String plan) throws JDOMException, IOException
    {
        if (this.plan == null) {
            this.plan = new PDDLPlan(plan);
            return new String[]{Double.toString(this.plan.getPlanningTime()), Integer.toString(this.plan.getNumberOfActions())};
        }
        else {
            return this.plan.updatePlan(plan);
        }
    }
    /**
     * This function compare a pddl state with the current state or with the next
     * state generated after the next action of the current plan is applicates
     * @param stateH
     * @param useNextState Define the state will be used in the comparation. If it is true, it will
     * be used next state and if it is false it will be used current state.
     * @param saveState
     * @param validateState
     * @return true if both state are similar and false if they are not
     */
    public boolean isValidState(String stateH, boolean useNextState, boolean saveState, boolean validateState)
    {
        NodeList temporal = new NodeList();
        boolean equalStates;
        
        if (validateState) {
            try
            {
                SAXBuilder builder      = new SAXBuilder();
                Document document       = (Document) builder.build(new StringReader(stateH));
                Element state           = document.getRootElement();
                List predicates         = ((Element) state.getChildren().get(1)).getChildren();

               for (int i = 0; i < predicates.size(); i++) {
                   temporal.addNode((Element) predicates.get(i));
               }
               
               equalStates = (this.comparison.getType() == PDDLComparison.EFFECTS) ? this.comparison.compare(this.problem.getNextPreconditions(), temporal):this.comparison.compare(this.problem.getNextState(), temporal);
               
               if (equalStates) {
                    if (saveState) {
                       this.problem.modifyState(temporal);
                    }
                    this.cursor++;
                    return true;
               }
               else 
                   return false;
            }
            catch (Exception e) {
               Util.printError(this.getName(), "Analizing State");
               return false;
            }
        }
        else {
            this.cursor++;
            return true;
        }
    }
    
    /**
     * This funcion generate a new state using the next action and save the new state
     * in the value newState.
     */
    public void generateNextState()
    {
        ActionPlan ap = this.plan.getAction(this.cursor);
        
        for (int i = 0; i < ap.size(); i++) {
            Action a = ap.get(i);
            this.problem.generateNextState(this.domain.generateEffectsByAction(a.getName(), a.getValues(), this.problem.getState()));
        }
        
        int nextAction = this.cursor + 1;
        
        if (nextAction < this.plan.getNumberOfActions()) {
            ap = this.plan.getAction(nextAction);
            
            for (int i = 0; i < ap.size(); i++) {
                Action a = ap.get(i);
                this.problem.generateNextPreconditions(this.domain.generatePreconditionsByAction(a.getName(), a.getValues(), this.problem.getState()));
            }
        }
        else {
            this.problem.generateNextPreconditions(new ArrayList<ActionResult>()); 
        }
    }
    
    /**
     * This function return the next action that will be executed. 
     * @return The next action of the plan
     */
    public ActionPlan getNextAction()
    {
        return this.plan.getAction(this.cursor);
    }
    
    public void deletePlan()
    {
        this.plan.deleteActions(this.cursor);
    }

    /**
     * This function analyze the current state 
     * @return Return true if goals has been reached and false if they are not. 
     */
    public boolean goalsReached()
    {
        return this.problem.goalsReached();
    }
    
    public String getDomainName() {
        return this.domain.getName();
    }
    
    public String getDomainXML() {
        return this.domain.generateDomainXML(false);
    }
    
    public String getProblemName() {
        return this.problem.getName();
    }
    
    public String getProblemXML()
    {
        return this.problem.generateProblemXML(false);
    }
    
    public String getPlanXML() {
        return this.plan.generateXML();
    }
    
    public String getPlanXML(int position) {
        return this.plan.generateXML(position);
    }
       
    public boolean moreActions()
    {
        return (this.plan != null) ?  (this.cursor < this.plan.getNumberOfActions()):false;
    }
    
    /**
     * This function return the number of actions that has been executed and monitoring
     * in a simulated environment.
     * @return Return the number of actions that has been executed correctly. 
     */
    public int actionsExecuted()
    {
        return this.cursor;
    }
    
    public boolean isPlan()
    {
        return ((this.plan != null) && (this.plan.getNumberOfActions() > 0));
    }
    
    public void loadState(String state) throws IOException, JDOMException
    {
        this.problem.loadState(state);
    }      
    
    public void loadDomain(String domain)
    {
        this.domain.loadDomain(domain);
    }
    
    public void resetPlan() {
        this.cursor = 0;
        this.plan = null;
    }
}
